/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kobakei.ratethisapp.sample.slice;

import com.kobakei.ratethisapp.RateThisApp;
import com.kobakei.ratethisapp.sample.ResourceTable;
import com.kobakei.ratethisapp.sample.utils.ToastViewDialog;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * MainAbilitySlice
 *
 * @since 2021-05-25
 */
public class MainAbilitySlice extends AbilitySlice {
    private static final String TAG = RateThisApp.class.getSimpleName();
    static final HiLogLabel LOG_LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, TAG);
    private static final boolean DEBUG = true;
    /**
     * 设定色彩style  顺序分别为 按键色，文字颜色，背景色
     */
    private static final RgbColor[] DEFAUTSTYLES = new RgbColor[]{
            new RgbColor(127, 140, 141),
            new RgbColor(189, 195, 199),
            new RgbColor(231, 76, 60)};

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initViews();
    }

    private void initViews() {
        // Set custom criteria (optional)
        RateThisApp.init(new RateThisApp.Config(3, 5));

        // Set callback (optional)
        RateThisApp.setCallback(new RateThisApp.Callback() {
            @Override
            public void onYesClicked() {
                ToastViewDialog.toast(MainAbilitySlice.this, "Yes event");
            }

            @Override
            public void onNoClicked() {
                ToastViewDialog.toast(MainAbilitySlice.this, "No event");
            }

            @Override
            public void onCancelClicked() {
                ToastViewDialog.toast(MainAbilitySlice.this, "Cancel event");
            }
        });

        /*
        // Set custom title and message
        RateThisApp.Config config = new RateThisApp.Config(3, 5);
        config.setTitle(R.string.hello_world);
        config.setMessage(R.string.hello_world);
        RateThisApp.init(config);
        */

        Button button1 = (Button) findComponentById(ResourceTable.Id_button1);
        button1.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                // Show rating dialog explicitly.
                RateThisApp.showRateCommonDialog(MainAbilitySlice.this, DEFAUTSTYLES);
            }
        });

        Button button2 = (Button) findComponentById(ResourceTable.Id_button2);
        button2.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                // Show rating dialog explicitly.
                RgbColor[] colors = new RgbColor[]{
                        new RgbColor(241, 196, 15),
                        new RgbColor(236, 240, 241),
                        new RgbColor(41, 128, 185)
                };
                RateThisApp.showRateCommonDialog(MainAbilitySlice.this, colors);
            }
        });

        Button button3 = (Button) findComponentById(ResourceTable.Id_button3);
        button3.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                log("button3");
                RateThisApp.stopRateDialog(MainAbilitySlice.this);
            }
        });

        // Monitor launch times and interval from installation
        RateThisApp.onCreate(this);

        // Show a dialog if criteria is satisfied
        RateThisApp.showRateDialogIfNeeded(this);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    /**
     * Print log if enabled
     *
     * @param message
     */
    private static void log(String message) {
        if (DEBUG) {
            HiLog.debug(LOG_LABEL, message);
        }
    }
}
