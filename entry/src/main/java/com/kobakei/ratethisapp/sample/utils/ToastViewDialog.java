/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kobakei.ratethisapp.sample.utils;

import com.kobakei.ratethisapp.sample.ResourceTable;

import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

/**
 * ToastViewDialog
 *
 * @since 2021-03-29
 *
 */
public class ToastViewDialog {
    private static int radius = 58;
    Context context;

    /**
     * toast
     *
     * @param context
     * @param text
     */
    public static void toast(Context context, String text) {
        toastDialog(context,"" + text);
    }

    /**
     * toastCtx
     *
     * @param ctx
     * @param text
     */
    public static void toastCtx(Context ctx, String text) {
        new ToastDialog(ctx)
                .setText(text)
                .setSize(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT)
                .setAutoClosable(true)
                .setDuration(5000)
                .setCornerRadius(radius)
                .show();
    }

    /**
     * toastCtx
     *
     * @param ctx
     * @param layout
     */
    public static void toastCtx(Context ctx, Component layout) {
        Component customToastLayout = (Component) LayoutScatter.getInstance(ctx).parse(layout.getId(), null, false);
        ToastDialog toastDialog = new ToastDialog(ctx);
        toastDialog.setComponent(customToastLayout);
        toastDialog.setCornerRadius(radius);
        toastDialog.setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastDialog.setAlignment(LayoutAlignment.CENTER);
        toastDialog.show();
    }

    /**
     * toast
     *
     * @param text
     */
    public void toast(String text) {
        new ToastDialog(context)
                .setText(text)
                .setSize(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT)
                .setAutoClosable(true)
                .setDuration(5000)
                .setCornerRadius(radius)
                .show();
    }

    /**
     * toast
     *
     * @param text
     * @param duration
     */
    public void toast(String text, int duration) {
        new ToastDialog(context)
                .setText(text)
                .setSize(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT)
                .setDuration(duration)
                .setCornerRadius(radius)
                .show();
    }

    /**
     * toast
     *
     * @param text
     * @param duration
     * @param offsetX
     * @param offsetY
     * @param gravity
     */
    public void toast(String text, int duration, int offsetX, int offsetY, int gravity) {
        new ToastDialog(context)
                .setText(text)
                .setSize(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT)
                .setAlignment(gravity)
                .setDuration(duration)
                .setCornerRadius(radius)
                .setOffset(offsetX, offsetY)
                .show();
    }

    /**
     * toast
     *
     * @param context
     * @param text
     */
    public static void toastDialog(Context context, String text) {
        ComponentContainer customToastLayout = (ComponentContainer) LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_toast_dialog_layout, null, false);

        Text textView = (Text) customToastLayout.findComponentById(ResourceTable.Id_toast_text);

        textView.setText(text);

        DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig(
                DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        customToastLayout.setLayoutConfig(layoutConfig);

        ToastDialog toastDialog = new ToastDialog(context);
        toastDialog.setComponent(customToastLayout);
        toastDialog.setCornerRadius(radius);
        toastDialog.setDuration(5000);
        toastDialog.setSize(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastDialog.setOffset(0,50);
        toastDialog.show();
    }
}
