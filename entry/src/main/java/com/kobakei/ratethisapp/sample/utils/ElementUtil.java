/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kobakei.ratethisapp.sample.utils;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ElementUtil
 *
 * @since 2021-05-25
 */
public class ElementUtil {

    /**
     * 获取ShapeElement,用于设置背景
     *
     * @param bgColor rgba背景色
     * @return 纯色背景
     */
    public static ShapeElement getShapeElement(int bgColor) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(bgColor));
        return shapeElement;
    }

    /**
     * 通过资源id获取color.json文件中的颜色
     *
     * @param context    上下文
     * @param resColorId 颜色的资源id
     * @return 颜色值
     */
    public static int getColor(Context context, int resColorId) {
        try {
            return context.getResourceManager().getElement(resColorId).getColor();
        } catch (IOException e) {
            Logger.getLogger(ElementUtil.class.getName()).log(Level.SEVERE, e.getMessage());
        } catch (NotExistException e) {
            Logger.getLogger(ElementUtil.class.getName()).log(Level.SEVERE, e.getMessage());
        } catch (WrongTypeException e) {
            Logger.getLogger(ElementUtil.class.getName()).log(Level.SEVERE, e.getMessage());
        }
        return 0x000000;
    }
}
