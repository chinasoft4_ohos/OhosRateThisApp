# OhosRateThisApp


#### 项目介绍
- 项目名称：OhosRateThisApp
- 所属系列：openharmony的第三方组件适配移植
- 功能：ohos库显示“为该应用评分”对话框
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release v1.2.0

#### 效果演示
![demo](https://images.gitee.com/uploads/images/2021/0616/093843_9f21e7c3_8751121.gif "demo.gif")

#### 安装教程
                                
1.在项目根目录下的build.gradle文件中，
 ```gradle

	allprojects {
	    repositories {
	        maven {
	            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
	        }
	    }
	}
 ```

2.在entry模块的build.gradle文件中，
 ```gradle

	 dependencies {
	    implementation('com.gitee.chinasoft_ohos:ohos_RateThisApp:1.0.0')
	    ......  
	 }
```

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

使用该库非常简单，只需查看提供的示例的源代码。（查看MainAbilitySlice.java中在initViews()中的使用）

```初始化java

	// Set custom criteria (optional)
        RateThisApp.init(new RateThisApp.Config(3, 5));

        // Set callback (optional)
        RateThisApp.setCallback(new RateThisApp.Callback() {
            @Override
            public void onYesClicked() {
                ToastViewDialog.toast(MainAbilitySlice.this, "Yes event");
            }

            @Override
            public void onNoClicked() {
                ToastViewDialog.toast(MainAbilitySlice.this, "No event");
            }

            @Override
            public void onCancelClicked() {
                ToastViewDialog.toast(MainAbilitySlice.this, "Cancel event");
            }
        });
```

```示例使用java

		RgbColor[] colors = new RgbColor[]{
                        new RgbColor(241, 196, 15),
                        new RgbColor(236, 240, 241),
                        new RgbColor(41, 128, 185)
                };
        RateThisApp.showRateCommonDialog(MainAbilitySlice.this, colors);

		RateThisApp.Config config = new RateThisApp.Config();
		config.setUrl("http://www.example.com");
		RateThisApp.init(config);

```



#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代

1.0.0

#### 版权和许可信息

	Copyright 2013-2017 Keisuke Kobayashi
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	    http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
