/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kobakei.utils;

import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;

/**
 * PreferencesUtil
 *
 * @since 2021-04-27
 */
public class PreferencesUtil {
    private static Preferences preferences;

    /**
     * Preferences
     *
     * @param context context
     * @return NetworkInfo
     */
    public static Preferences getInstance(Context context) {
        if (preferences == null) {
            preferences = new DatabaseHelper(context).getPreferences(context.getPreferencesDir().getName());
        }
        return preferences;
    }
}
