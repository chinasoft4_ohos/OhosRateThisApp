package com.kobakei.utils;

import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * author ：Administrator
 * date : 2021/5/28 9:30
 * package：com.kobakei.utils
 * description :
 */
public class TextUtilsTest extends TestCase {
    //（全UI应用、不支持Context等等原因）不支持单元测试
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.kobakei.ratethisapp.sample", actualBundleName);
    }
}