package com.kobakei.ratethisapp;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleTest {
    //（全UI应用、不支持Context等等原因）不支持单元测试
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.kobakei.ratethisapp.sample", actualBundleName);
    }
}
